+++
title = "AI roadmap"
date = 2023-09-21T09:56:00+08:00
lastmod = 2024-06-06T16:08:11+08:00
categories = ["ai"]
draft = false
toc = true
image = "https://r2.guolongji.xyz/allinaent/2024/06/3bd4bc0b766ce3d644039524b8963a45.jpg"
+++

目录渲染不要设置为 pandoc，gitlab ci 可以不用 pandoc 了吧。否则目录不显示 3 级以上的标题。还有，也不知道是 ox-hugo 的问题还是 hugo 本身的问题，orgmode 转 md，orgmode 的不要使用二级的标题，否则生成的 md 标题层次不对。2，3级同层。


## AI 必备基础与机器学习 {#ai-必备基础与机器学习}


### Python 基础及其工具包实战 {#python-基础及其工具包实战}


#### Python 快速入⻔ {#python-快速入}


#### 科学计算库-Numpy {#科学计算库-numpy}


#### 数据分析处理库-Pandas {#数据分析处理库-pandas}


#### 可视化库-Matplotlib {#可视化库-matplotlib}


#### 可视化库-Seaborn {#可视化库-seaborn}


### 人工智能必备数学基础 {#人工智能必备数学基础}


#### 高等数学基础 {#高等数学基础}


#### 微积分 {#微积分}


#### 泰勒公式与拉格朗日 {#泰勒公式与拉格朗日}


#### 线性代数基础 {#线性代数基础}


#### 特征值与矩阵分解 {#特征值与矩阵分解}


#### 随机变量 {#随机变量}


#### 概率论基础 {#概率论基础}


#### 数据科学你得知道的几种分布 {#数据科学你得知道的几种分布}


#### 核函数变换 {#核函数变换}


#### 熵与激活函数 {#熵与激活函数}


#### 回归分析 {#回归分析}


#### 假设检验 {#假设检验}


#### 相关分析 {#相关分析}


#### 方差分析 {#方差分析}


#### 聚类分析 {#聚类分析}


#### ⻉叶斯分析 {#叶斯分析}


### 第三模块机器学习算法精讲及其案例应用 {#第三模块机器学习算法精讲及其案例应用}


#### 线性回归原理推导 {#线性回归原理推导}


#### 线性回归代码实现 {#线性回归代码实现}


#### 模型评估方法 {#模型评估方法}


#### 线性回归实验分析 {#线性回归实验分析}


#### 逻辑回归代码实现 {#逻辑回归代码实现}


#### 逻辑回归实验分析 {#逻辑回归实验分析}


#### 聚类算法-Kmeans&amp;Dbscan 原理 {#聚类算法-kmeans-and-dbscan-原理}


#### Kmeans 代码实现 {#kmeans-代码实现}


#### 聚类算法实验分析 {#聚类算法实验分析}


#### 决策树原理 {#决策树原理}


#### 决策树代码实现 {#决策树代码实现}


#### 决策树实验分析 {#决策树实验分析}


#### 集成算法原理 {#集成算法原理}


#### 集成算法实验分析 {#集成算法实验分析}


#### 支持向量机原理推导 {#支持向量机原理推导}


#### 支持向量机实验分析 {#支持向量机实验分析}


#### 神经网络算法原理 {#神经网络算法原理}


#### 神经网络代码实现 {#神经网络代码实现}


#### ⻉叶斯算法原理 {#叶斯算法原理}


#### ⻉叶斯代码实现 {#叶斯代码实现}


#### 关联规则实战分析 {#关联规则实战分析}


#### 关联规则代码实现 {#关联规则代码实现}


#### 词向量 word2vec 通俗解读 {#词向量-word2vec-通俗解读}


#### 代码实现 word2vec 词向量模型 {#代码实现-word2vec-词向量模型}


#### 推荐系统原理 {#推荐系统原理}


#### 打造音乐推荐系统 {#打造音乐推荐系统}


#### 主成分分析 {#主成分分析}


#### 线性判别分析 {#线性判别分析}


#### 主成分分析降维算法解读 {#主成分分析降维算法解读}


#### 隐⻢尔科夫模型 {#隐-尔科夫模型}


#### HIIM 应用实例 {#hiim-应用实例}


### 机器学习&amp;数据挖掘项目实战 {#机器学习-and-数据挖掘项目实战}


#### 机器学习 {#机器学习}


#### Python 实战关联规则 {#python-实战关联规则}


#### 爱彼迎数据集分析与建模 {#爱彼迎数据集分析与建模}


#### 基于相似度的酒店推荐系统 {#基于相似度的酒店推荐系统}


#### 商品销售额回归分析 {#商品销售额回归分析}


#### 绝地求生数据集探索分析与建模 {#绝地求生数据集探索分析与建模}


#### 机器学习-模型解释方法实战 {#机器学习-模型解释方法实战}


#### NLP 常用工具包 {#nlp-常用工具包}


#### NLP 核心模型-word2vec {#nlp-核心模型-word2vec}


#### 自然语言处理特征提取方式对比 {#自然语言处理特征提取方式对比}


#### 数据特征预处理 {#数据特征预处理}


#### 银行客戶还款可能性预测 {#银行客戶还款可能性预测}


#### 图像特征聚类分析实践 {#图像特征聚类分析实践}


#### 数据挖掘 {#数据挖掘}


#### 快手用戶活跃度预测 {#快手用戶活跃度预测}


#### 工业化生产预测 {#工业化生产预测}


#### 智慧城市道路通行时间预测 {#智慧城市道路通行时间预测}


#### 特征工程建模可解释工具包 {#特征工程建模可解释工具包}


#### 医学糖尿病数据命名实体识别 {#医学糖尿病数据命名实体识别}


#### 贷款平台⻛控模型-特征工程 {#贷款平台-控模型-特征工程}


#### 新闻关键词抽取模型 {#新闻关键词抽取模型}


#### 数据特征常用构建方法 {#数据特征常用构建方法}


#### 用电敏感客戶分类 {#用电敏感客戶分类}


#### 机器学习项目实战模板 {#机器学习项目实战模板}


### python 金融分析与量化交易实战 {#python-金融分析与量化交易实战}


#### 金融时间序列分析 {#金融时间序列分析}


#### 双均线交易策略实例 {#双均线交易策略实例}


#### 策略评估指标 {#策略评估指标}


#### 量化交易解读 {#量化交易解读}


#### Ricequant 回测选股分析实战 {#ricequant-回测选股分析实战}


#### 因子数据预处理 {#因子数据预处理}


#### 因子策略选股实例 {#因子策略选股实例}


#### 因子分析实战 {#因子分析实战}


#### 因子打分选股实例 {#因子打分选股实例}


#### 基于回归的策略分析 {#基于回归的策略分析}


#### 聚类与统计策略分析 {#聚类与统计策略分析}


#### Fbprophet 时间测序预测神器 {#fbprophet-时间测序预测神器}


#### 深度学习时间序列预测 {#深度学习时间序列预测}


#### Matplotlib 工具包实战 {#matplotlib-工具包实战}


#### Seaborn {#seaborn}


## 计算机视觉算法及其项目实战 {#计算机视觉算法及其项目实战}


### 深度学习必备核心算法 {#深度学习必备核心算法}


#### 神经网络必备基础知识点 {#神经网络必备基础知识点}


#### 神经网络前向传播与反向传播 {#神经网络前向传播与反向传播}


#### 网络模型整体架构分析实例 {#网络模型整体架构分析实例}


#### 神经网络建模效果分析 {#神经网络建模效果分析}


#### 激活函数与过拟合问题解决 {#激活函数与过拟合问题解决}


#### 卷积神经网络核心知识点 {#卷积神经网络核心知识点}


#### 卷积建模流程与各参数作用分析 {#卷积建模流程与各参数作用分析}


#### 池化层的作用与效果 {#池化层的作用与效果}


#### 经典卷积神经网络架构分析 {#经典卷积神经网络架构分析}


#### 感受野的作用与效果解读 {#感受野的作用与效果解读}


#### 递归神经网络模型原理分析 {#递归神经网络模型原理分析}


#### RNN 系列网络结构优缺点分析 {#rnn-系列网络结构优缺点分析}


#### 词向量模型与 LSTM 应用实例 {#词向量模型与-lstm-应用实例}


### 深度学习核心框架 PyTorch 与 Tensorflow {#深度学习核心框架-pytorch-与-tensorflow}


#### 深度学习框架 PyTorch {#深度学习框架-pytorch}


#### PyTorch 框架基本处理操作 {#pytorch-框架基本处理操作}


#### 神经网络实战分类与回归任务 {#神经网络实战分类与回归任务}


#### 卷积神经网络原理与参数解读 {#卷积神经网络原理与参数解读}


#### 图像识别核心模块实战解读 {#图像识别核心模块实战解读}


#### 迁移学习的作用与应用实例 {#迁移学习的作用与应用实例}


#### 计算机视觉模型通用训练策略 {#计算机视觉模型通用训练策略}


#### Dataloader 构建自己的数据集 {#dataloader-构建自己的数据集}


#### Torch 框架自定义类与数据模块 {#torch-框架自定义类与数据模块}


#### 递归神经网络与词向量原理解读 {#递归神经网络与词向量原理解读}


#### 新闻数据集文本分类实战 {#新闻数据集文本分类实战}


#### 深度学习框架 Tensorflow {#深度学习框架-tensorflow}


#### tensorflow 安装与简介 {#tensorflow-安装与简介}


#### 神经网络原理解读与整体架构 {#神经网络原理解读与整体架构}


#### 搭建神经网络进行分类与回归任务 {#搭建神经网络进行分类与回归任务}


#### 卷积神经网络原理与参数解读 {#卷积神经网络原理与参数解读}


#### 猫狗识别实战 {#猫狗识别实战}


#### 图像数据增强实例 {#图像数据增强实例}


#### 训练策略-迁移学习实战 {#训练策略-迁移学习实战}


#### 递归神经网络与词向量原理解读 {#递归神经网络与词向量原理解读}


#### 基于 TensorFlow 实现 word2vec {#基于-tensorflow-实现-word2vec}


#### 基于 RNN 模型进行文本分类任务 {#基于-rnn-模型进行文本分类任务}


#### 将 CNN 网络应用于文本分类实战 {#将-cnn-网络应用于文本分类实战}


#### 时间序列预测 {#时间序列预测}


#### 经典网络架构 Resnet 实战 {#经典网络架构-resnet-实战}


### Opencv 图像处理框架实战 {#opencv-图像处理框架实战}


#### 课程简介与环境配置 {#课程简介与环境配置}


#### 图像基本操作 {#图像基本操作}


#### 阈值与平滑处理 {#阈值与平滑处理}


#### 图像形态学操作 {#图像形态学操作}


#### 图像梯度计算 {#图像梯度计算}


#### 边缘检测 {#边缘检测}


#### 图像金字塔与轮廓检测 {#图像金字塔与轮廓检测}


#### 直方图与傅里叶变换 {#直方图与傅里叶变换}


#### 信用卡数字识别 {#信用卡数字识别}


#### 文档扫描 OCR 识别 {#文档扫描-ocr-识别}


#### 图像特征-harris {#图像特征-harris}


#### 图像特征-sift {#图像特征-sift}


#### 全景图像拼接 {#全景图像拼接}


#### 停⻋场⻋位识别 {#停-场-位识别}


#### 答题卡识别判卷 {#答题卡识别判卷}


#### 背景建模 {#背景建模}


#### 光流估计 {#光流估计}


#### Opencv 的 DNN 模块 {#opencv-的-dnn-模块}


#### 目标追踪 {#目标追踪}


#### 卷积原理与操作 {#卷积原理与操作}


#### 疲劳检测 {#疲劳检测}


### 综合项目-物体检测经典算法实战 {#综合项目-物体检测经典算法实战}


#### 深度学习经典检测方法概述 {#深度学习经典检测方法概述}


#### YOLO-V1 整体思想与网络架构 {#yolo-v1-整体思想与网络架构}


#### YOLO-V2 改进细节详解 {#yolo-v2-改进细节详解}


#### YOLO-V3 核心网络模型 {#yolo-v3-核心网络模型}


#### 基于 V3 版本进行源码解读 {#基于-v3-版本进行源码解读}


#### 基于 YOLO-V3 训练自己的数据集与任务 {#基于-yolo-v3-训练自己的数据集与任务}


#### YOLO-V4 版本算法解读 {#yolo-v4-版本算法解读}


#### V5 版本项目配置 {#v5-版本项目配置}


#### V5 项目工程源码解读 {#v5-项目工程源码解读}


#### Efficient net 模块解读 {#efficient-net-模块解读}


#### Efficient Det 目标检测框架算法分析 {#efficient-det-目标检测框架算法分析}


#### Efficient Det 源码分析与应用 {#efficient-det-源码分析与应用}


#### Detr 目标检测算法解读 {#detr-目标检测算法解读}


#### Detr 源码分析与应用实战 {#detr-源码分析与应用实战}


#### Deformable DETR 算法改进解读 {#deformable-detr-算法改进解读}


#### Deformable DETR 源码解读与架构分析 {#deformable-detr-源码解读与架构分析}


#### Fcos 算法如何提升检测速度 {#fcos-算法如何提升检测速度}


#### 可变形卷积效果分析与行人检测实例 {#可变形卷积效果分析与行人检测实例}


### 图像分割实战 {#图像分割实战}


#### 图像分割及其损失函数概述 {#图像分割及其损失函数概述}


#### 卷积神经网络原理与参数解读 {#卷积神经网络原理与参数解读}


#### Unet 系列算法讲解 {#unet-系列算法讲解}


#### unet 医学细胞分割实战 {#unet-医学细胞分割实战}


#### U2NET 显著性检测实战 {#u2net-显著性检测实战}


#### deeplab 系列算法 {#deeplab-系列算法}


#### 基于 deeplabV3+版本进行 VOC 分割实战 {#基于-deeplabv3-plus-版本进行-voc-分割实战}


#### 医学心脏视频数据集分割建模实战 {#医学心脏视频数据集分割建模实战}


#### Medical Transformer 分割实战 {#medical-transformer-分割实战}


#### Medical Transformer 源码实现与应用实例 {#medical-transformer-源码实现与应用实例}


#### 可学习位置编码模块对分割算法的作用分析 {#可学习位置编码模块对分割算法的作用分析}


#### 分割任务如何标注与训练自己的项目 {#分割任务如何标注与训练自己的项目}


### 走向 AI 论文实验与项目实战的捷径-MMLAB 实战系列 {#走向-ai-论文实验与项目实战的捷径-mmlab-实战系列}


#### MMLAB 系列环境搭建方法(以不变应万变) {#mmlab-系列环境搭建方法--以不变应万变}


#### MMCLS 分类模块核心功能解读 {#mmcls-分类模块核心功能解读}


#### 图像识别配置文件作用与功能解读 {#图像识别配置文件作用与功能解读}


#### 图像识别任务标注方法与实例 {#图像识别任务标注方法与实例}


#### 如何使用 MMCLS 构建自己分类数据集 {#如何使用-mmcls-构建自己分类数据集}


#### 基于配置文件进行模型训练与测试任务 {#基于配置文件进行模型训练与测试任务}


#### 可视化分析与评估指标生成方法 {#可视化分析与评估指标生成方法}


#### MMSEG 图像分割模块核心功能解读 {#mmseg-图像分割模块核心功能解读}


#### 图像分割配置文件作用与功能解读 {#图像分割配置文件作用与功能解读}


#### 图像分割标注方法与实例 {#图像分割标注方法与实例}


#### 如何使用 MMSEG 构建自己的分割数据集 {#如何使用-mmseg-构建自己的分割数据集}


#### 替换配置文件核心组件开启自己的实验任务 {#替换配置文件核心组件开启自己的实验任务}


#### CVPR 最新分割模块核心 backbone 解读 {#cvpr-最新分割模块核心-backbone-解读}


#### 源码分析 MMSEG 核心项目模块 {#源码分析-mmseg-核心项目模块}


#### MMDET 物体检测模块核心功能解读 {#mmdet-物体检测模块核心功能解读}


#### 物体检测配置文件作用与功能实例 {#物体检测配置文件作用与功能实例}


#### 物体检测标注方法与应用实例 {#物体检测标注方法与应用实例}


#### 使用 MMDET 训练自己的数据集与任务 {#使用-mmdet-训练自己的数据集与任务}


#### Deformable DETR 算法与论文分析 {#deformable-detr-算法与论文分析}


#### Deformable DETR 源码解读实战 {#deformable-detr-源码解读实战}


#### MMOCR 核心模块解读 {#mmocr-核心模块解读}


#### OCR 中三大核心任务:检测,识别,抽取 {#ocr-中三大核心任务-检测-识别-抽取}


#### DBNET 文字检测算法与项目实战 {#dbnet-文字检测算法与项目实战}


#### ANINET 文字识别算法与项目实战 {#aninet-文字识别算法与项目实战}


#### KIE 基于图模型的关键信息抽取算法及其实战 {#kie-基于图模型的关键信息抽取算法及其实战}


#### MMGAN 对抗生成网络核心模块解读 {#mmgan-对抗生成网络核心模块解读}


#### StyleGan 系列算法架构分析 {#stylegan-系列算法架构分析}


#### StyleGan 源码解读与应用 {#stylegan-源码解读与应用}


#### MMEDIT 核心模块解读 {#mmedit-核心模块解读}


#### BasicVSR++视频超分辨重构算法与论文分析 {#basicvsr-plus-plus-视频超分辨重构算法与论文分析}


#### BasucVSR++源码分析与网络架构解读 {#basucvsr-plus-plus-源码分析与网络架构解读}


#### MMRAZOR 模型蒸馏与剪枝模块解读 {#mmrazor-模型蒸馏与剪枝模块解读}


#### 模型蒸馏算法及其应用实例 {#模型蒸馏算法及其应用实例}


#### 模型剪枝算法及其应用实例 {#模型剪枝算法及其应用实例}


### 行为识别实战 {#行为识别实战}


#### slowfast 算法知识点通俗解读 {#slowfast-算法知识点通俗解读}


#### slowfast 项目环境配置与配置文件 {#slowfast-项目环境配置与配置文件}


#### slowfast 源码详细解读 {#slowfast-源码详细解读}


#### 基于 3D 卷积的视频分析与动作识别 {#基于-3d-卷积的视频分析与动作识别}


#### 视频异常检测算法与元学习 {#视频异常检测算法与元学习}


#### CVPR2021 视频异常检测论文及其源码解读 {#cvpr2021-视频异常检测论文及其源码解读}


### 于 2022 论文必备-Transformer 实战系列 {#于-2022-论文必备-transformer-实战系列}


#### Transformer 基本架构解读 {#transformer-基本架构解读}


#### 注意力机制的作用与效果分析 {#注意力机制的作用与效果分析}


#### 源码解读 Transformer 整体架构实现 {#源码解读-transformer-整体架构实现}


#### Transformer 在视觉中的应用 VIT 算法 {#transformer-在视觉中的应用-vit-算法}


#### VIT 分类 Backbone 算法模型源码解读 {#vit-分类-backbone-算法模型源码解读}


#### SwinTransformer 算法原理解析 {#swintransformer-算法原理解析}


#### SwinTransformer 源码解读 {#swintransformer-源码解读}


#### 基于 Transformer 的 Detr 目标检测算法 {#基于-transformer-的-detr-目标检测算法}


#### 物体检测新型框架 Detr 源码解读 {#物体检测新型框架-detr-源码解读}


#### Deformable DETR 算法与论文分析 {#deformable-detr-算法与论文分析}


#### Deformable DETR 源码分析与架构实现 {#deformable-detr-源码分析与架构实现}


#### 基于 Transformer 的关键点匹配 Loftr {#基于-transformer-的关键点匹配-loftr}


#### 商汤 Loftr 源码解读与应用实例 {#商汤-loftr-源码解读与应用实例}


#### Medical Transformer 应用于分割任务实例 {#medical-transformer-应用于分割任务实例}


#### Medical Transformer 源码分析与应用实例 {#medical-transformer-源码分析与应用实例}


#### Informer 时间序列预测任务解读 {#informer-时间序列预测任务解读}


#### Informer 源码解读与应用实战 {#informer-源码解读与应用实战}


#### 基于 Transformer 的 ANINET 文字识别 {#基于-transformer-的-aninet-文字识别}


#### 文字识别项目源码分析与应用实例 {#文字识别项目源码分析与应用实例}


#### 谷歌开源项目 BERT 源码解读与应用实例 {#谷歌开源项目-bert-源码解读与应用实例}


#### BERT 的中文情感分析实战 {#bert-的中文情感分析实战}


#### 最新热⻔Transformer 论文与实战解读 {#最新热-transformer-论文与实战解读}


### 图神经网络实战 {#图神经网络实战}


#### 图模型应用领域分析概述 {#图模型应用领域分析概述}


#### 图神经网络 GNN 原理与架构解读 {#图神经网络-gnn-原理与架构解读}


#### 图卷积 GCN 模块构建与效果 {#图卷积-gcn-模块构建与效果}


#### 图神经网络框架 PyTorch-Geometric 安装与配置 {#图神经网络框架-pytorch-geometric-安装与配置}


#### 图神经网络框架 PyTorch-Geometric 应用实战 {#图神经网络框架-pytorch-geometric-应用实战}


#### 使用 PyTorch-Geometric 搭建自己的图数据集 {#使用-pytorch-geometric-搭建自己的图数据集}


#### 图注意力机制与图序列网络模型解读 {#图注意力机制与图序列网络模型解读}


#### 图神经网络时间序列预测实战 {#图神经网络时间序列预测实战}


#### 图相似度计算与匹配算法解读 {#图相似度计算与匹配算法解读}


#### 化学分子相似度计算匹配实战 {#化学分子相似度计算匹配实战}


#### 基于图模型的 VectorNet 轨迹估计算法 {#基于图模型的-vectornet-轨迹估计算法}


#### VectorNet 应用实例与源码分析 {#vectornet-应用实例与源码分析}


#### 基于图模型的 KIE 关键信息抽取 {#基于图模型的-kie-关键信息抽取}


#### KIE 算法与图模型构建实战解读 {#kie-算法与图模型构建实战解读}


### 3D 点云实战 {#3d-点云实战}


#### 3D 点云应用领域分析 {#3d-点云应用领域分析}


#### 3D 点云 PointNet 算法 {#3d-点云-pointnet-算法}


#### PointNet++算法解读 {#pointnet-plus-plus-算法解读}


#### Pointnet++项目实战 {#pointnet-plus-plus-项目实战}


#### 点云补全 PF-Net 论文解读 {#点云补全-pf-net-论文解读}


#### 点云补全实战解读 {#点云补全实战解读}


#### 点云配准及其案例实战 {#点云配准及其案例实战}


### 目标追踪与姿态估计实战 {#目标追踪与姿态估计实战}


#### 姿态估计算法经典套路分析 {#姿态估计算法经典套路分析}


#### 姿态估计 OpenPose 论文分析 {#姿态估计-openpose-论文分析}


#### 姿态估计算法输出模块解读 {#姿态估计算法输出模块解读}


#### OpenPose 算法源码实战解读 {#openpose-算法源码实战解读}


#### 目标追踪要完成的任务分析 {#目标追踪要完成的任务分析}


#### deepsort 算法知识点解读 {#deepsort-算法知识点解读}


#### 卡尔曼滤波与状态估计实例 {#卡尔曼滤波与状态估计实例}


#### REID 在追踪任务中的作用 {#reid-在追踪任务中的作用}


#### deepsort 项目源码解读 {#deepsort-项目源码解读}


#### YOLOV5 与 Deepsort 追踪实战 {#yolov5-与-deepsort-追踪实战}


### 面向深度学习的无人驾驶实战 {#面向深度学习的无人驾驶实战}


#### 深度估计算法及其原理解读 {#深度估计算法及其原理解读}


#### 深度估计项目实战源码解读 {#深度估计项目实战源码解读}


#### ⻋道线检测论文及其算法实现 {#道线检测论文及其算法实现}


#### 基于深度学习的⻋道线检测项目实战 {#基于深度学习的-道线检测项目实战}


#### 轨迹预测实例与特征空间解读 {#轨迹预测实例与特征空间解读}


#### 基于 VectorNet 进行估计估计实战 {#基于-vectornet-进行估计估计实战}


#### 特征点匹配方法原理与论文分析 {#特征点匹配方法原理与论文分析}


#### 商汤最新特征点匹配算法实战 {#商汤最新特征点匹配算法实战}


#### 三维重建算法原理分析 {#三维重建算法原理分析}


#### TSDF 方法应用与解读 {#tsdf-方法应用与解读}


#### 商汤最新三维重建项目原理与源码解读 {#商汤最新三维重建项目原理与源码解读}


#### 特斯拉无人驾驶项目分析与技术细节解读 {#特斯拉无人驾驶项目分析与技术细节解读}


### 对比学习与多模态任务实战 {#对比学习与多模态任务实战}


#### 对比学习核心思想与应用领域分析 {#对比学习核心思想与应用领域分析}


#### 对比学习经典算法原理与架构解读 {#对比学习经典算法原理与架构解读}


#### 对比学习与多模态融合方法 {#对比学习与多模态融合方法}


#### Clip 算法流程分析与应用实例 {#clip-算法流程分析与应用实例}


#### 多模态 Clip 算法模型源码解读 {#多模态-clip-算法模型源码解读}


#### 文本与视觉多模态任务结合分析 {#文本与视觉多模态任务结合分析}


#### Abinet 文本与视觉融合 OCR 论文解读 {#abinet-文本与视觉融合-ocr-论文解读}


#### 文本视觉融合模型源码实战 {#文本视觉融合模型源码实战}


#### 3D 点云与图像数据融合论文解读 {#3d-点云与图像数据融合论文解读}


#### 3D 目标检测多模态融合源码解读与实例 {#3d-目标检测多模态融合源码解读与实例}


### 缺陷检测实战 {#缺陷检测实战}


#### 物体检测框架 YOLO-V4 版本算法解读 {#物体检测框架-yolo-v4-版本算法解读}


#### 物体检测框架 YOLO-V5 版本项目配置 {#物体检测框架-yolo-v5-版本项目配置}


#### 物体检测框架 YOLO-V5 项目工程源码解读 {#物体检测框架-yolo-v5-项目工程源码解读}


#### 基于 YOLOV5 的钢材缺陷检测实战 {#基于-yolov5-的钢材缺陷检测实战}


#### Semi-supervised 布料缺陷检测实战 {#semi-supervised-布料缺陷检测实战}


#### Opnecv 图像常用处理方法实例 {#opnecv-图像常用处理方法实例}


#### Opnecv 梯度计算与边缘检测实例 {#opnecv-梯度计算与边缘检测实例}


#### Opnecv 轮廓检测与直方图 {#opnecv-轮廓检测与直方图}


#### 基于 Opnecv 缺陷检测项目实战 {#基于-opnecv-缺陷检测项目实战}


#### 基于视频流水线的 Opnecv 缺陷检测项目 {#基于视频流水线的-opnecv-缺陷检测项目}


#### 图像分割 deeplab 系列算法 {#图像分割-deeplab-系列算法}


#### 基于 deeplabV3+版本进行 VOC 分割实战 {#基于-deeplabv3-plus-版本进行-voc-分割实战}


#### Deeplab 铁质材料缺陷检测与开源项目应用流程 {#deeplab-铁质材料缺陷检测与开源项目应用流程}


### 行人重识别实战 {#行人重识别实战}


#### 行人重识别原理及其应用 {#行人重识别原理及其应用}


#### 基于注意力机制的 ReId 模型论文解读 {#基于注意力机制的-reid-模型论文解读}


#### 基于 Attention 的行人重识别项目实战 {#基于-attention-的行人重识别项目实战}


#### AAAI 顶会算法与论文精讲 {#aaai-顶会算法与论文精讲}


#### 基于行人局部特征融合的再识别实战 {#基于行人局部特征融合的再识别实战}


#### 旷视研究院最新算法解读(基于图模型) {#旷视研究院最新算法解读--基于图模型}


#### 基于拓扑图的行人重识别项目实战 {#基于拓扑图的行人重识别项目实战}


#### CVPR 最新行人搜索算法与应用实例 {#cvpr-最新行人搜索算法与应用实例}


#### 行人搜索项目源码解读与效果分析 {#行人搜索项目源码解读与效果分析}


### 对抗生成网络实战 {#对抗生成网络实战}


#### 对抗生成网络架构原理与实战解析 {#对抗生成网络架构原理与实战解析}


#### 基于 CycleGan 开源项目实战图像合成 {#基于-cyclegan-开源项目实战图像合成}


#### stargan 论文架构解析 {#stargan-论文架构解析}


#### stargan 项目实战及其源码解读 {#stargan-项目实战及其源码解读}


#### 基于 starganvc2 的变声器论文原理解读 {#基于-starganvc2-的变声器论文原理解读}


#### starganvc2 变声器项目实战及其源码解读 {#starganvc2-变声器项目实战及其源码解读}


#### 图像超分辨率重构实战 {#图像超分辨率重构实战}


#### 基于 GAN 的图像补全实战 {#基于-gan-的图像补全实战}


#### StyleGan 人脸合成算法解读 {#stylegan-人脸合成算法解读}


#### StyleGan 项目源码解读实例 {#stylegan-项目源码解读实例}


#### BasicVSR++视频超分辨重构算法解读 {#basicvsr-plus-plus-视频超分辨重构算法解读}


#### BasicVSR++项目源码实战 {#basicvsr-plus-plus-项目源码实战}


### 强化学习实战系列 {#强化学习实战系列}


#### 强化学习简介及其应用 {#强化学习简介及其应用}


#### PPO 算法与公式推导 {#ppo-算法与公式推导}


#### PPO 实战-月球登陆器训练实例 {#ppo-实战-月球登陆器训练实例}


#### Q-learning 与 DQN 算法 {#q-learning-与-dqn-算法}


#### DQN 算法实例演示 {#dqn-算法实例演示}


#### DQN 改进与应用技巧 {#dqn-改进与应用技巧}


#### Actor-Critic 算法分析(A3C) {#actor-critic-算法分析--a3c}


#### 用 A3C 玩转超级⻢里奥 {#用-a3c-玩转超级-里奥}


### Openai 顶级黑科技算法及其项目实战 {#openai-顶级黑科技算法及其项目实战}


#### Openai 生成模型 GPT 系列算法解读 {#openai-生成模型-gpt-系列算法解读}


#### GPT 架构实现与生成模型训练源码分析 {#gpt-架构实现与生成模型训练源码分析}


#### 文本与视觉融合模型 CLIP 原理架构分析 {#文本与视觉融合模型-clip-原理架构分析}


#### Diffusion 扩散模型算法思想解读 {#diffusion-扩散模型算法思想解读}


#### 扩散模型与 GAN 对比分析及其公式推导 {#扩散模型与-gan-对比分析及其公式推导}


#### 扩散模型源码实现与效果分析 {#扩散模型源码实现与效果分析}


#### 黑科技算法 Dalle 系列基本原理与架构分析 {#黑科技算法-dalle-系列基本原理与架构分析}


#### Dalle2 源码解读及其应用流程实现 {#dalle2-源码解读及其应用流程实现}


### 面向医学领域的深度学习实战 {#面向医学领域的深度学习实战}


#### 卷积神经网络原理与参数解读 {#卷积神经网络原理与参数解读}


#### PyTorch 框架基本处理操作 {#pytorch-框架基本处理操作}


#### PyTorch 框架必备核心模块解读 {#pytorch-框架必备核心模块解读}


#### 基于 Resnet 的医学数据集分类实战 {#基于-resnet-的医学数据集分类实战}


#### 图像分割及其损失函数概述 {#图像分割及其损失函数概述}


#### Unet 系列算法讲解 {#unet-系列算法讲解}


#### Unet 医学细胞分割实战 {#unet-医学细胞分割实战}


#### deeplab 系列算法 {#deeplab-系列算法}


#### 基于 deeplabV3+版本进行 VOC 分割实战 {#基于-deeplabv3-plus-版本进行-voc-分割实战}


#### 基于 deeplab 的心脏视频数据诊断分析 {#基于-deeplab-的心脏视频数据诊断分析}


#### YOLO 系列物体检测算法原理解读 {#yolo-系列物体检测算法原理解读}


#### 基于 YOLO5 细胞检测实战 {#基于-yolo5-细胞检测实战}


#### 知识图谱原理解读 {#知识图谱原理解读}


#### Neo4j 数据库实战 {#neo4j-数据库实战}


#### 基于知识图谱的医药问答系统实战 {#基于知识图谱的医药问答系统实战}


#### 词向量模型与 RNN 网络架构 {#词向量模型与-rnn-网络架构}


#### 医学糖尿病数据命名实体识别 {#医学糖尿病数据命名实体识别}


### 深度学习模型部署与剪枝优化实战 {#深度学习模型部署与剪枝优化实战}


#### PyTorch 框架部署实践 {#pytorch-框架部署实践}


#### YOLO 物体检测部署实例 {#yolo-物体检测部署实例}


#### docker 实例演示 {#docker-实例演示}


#### tensorflflow-serving 实战 {#tensorflflow-serving-实战}


#### 模型减枝-NetworkSlimming 算法分析 {#模型减枝-networkslimming-算法分析}


#### 模型减枝-NetworkSlimming 实战解读 {#模型减枝-networkslimming-实战解读}


#### 轻量化模型 Mobilenet 三代网络模型架构 {#轻量化模型-mobilenet-三代网络模型架构}


#### Mobilenet 系列源码与应用实例 {#mobilenet-系列源码与应用实例}


#### 模型蒸馏算法与应用实例 {#模型蒸馏算法与应用实例}


#### 模型蒸馏在分割任务中的实战 {#模型蒸馏在分割任务中的实战}


#### 模型加速与格式转换实战 {#模型加速与格式转换实战}


#### CV 各领域经典项目部署实例 {#cv-各领域经典项目部署实例}


## 自然语言处理算法及其项目实战 {#自然语言处理算法及其项目实战}


### 自然语言处理经典案例实战 {#自然语言处理经典案例实战}


#### NLP 常用工具包实战 {#nlp-常用工具包实战}


#### 商品信息可视化与文本分析 {#商品信息可视化与文本分析}


#### ⻉叶斯算法 {#叶斯算法}


#### 新闻分类任务实战 {#新闻分类任务实战}


#### HMM 隐⻢尔科夫模型 {#hmm-隐-尔科夫模型}


#### HMM 工具包实战 {#hmm-工具包实战}


#### 语言模型 {#语言模型}


#### 使用 Gemsim 构建词向量 {#使用-gemsim-构建词向量}


#### 基于 word2vec 的分类任务 {#基于-word2vec-的分类任务}


#### NLP-文本特征方法对比 {#nlp-文本特征方法对比}


#### NLP-相似度模型 {#nlp-相似度模型}


#### LSTM 情感分析 {#lstm-情感分析}


#### 机器人写唐诗 {#机器人写唐诗}


#### 对话机器人 {#对话机器人}


### 自然语言处理必备神器 Huggingface 系列实战 {#自然语言处理必备神器-huggingface-系列实战}


#### Huggingface 为 NLP 领域带来的变革与价值 {#huggingface-为-nlp-领域带来的变革与价值}


#### Huggingface 各核心模块使用方法及其应用流程 {#huggingface-各核心模块使用方法及其应用流程}


#### MLM 完形填空任务解读及其训练应用实例 {#mlm-完形填空任务解读及其训练应用实例}


#### 文本摘要任务构建流程及其训练测试实例 {#文本摘要任务构建流程及其训练测试实例}


#### 预训练模型的作用及其下游任务微调方法 {#预训练模型的作用及其下游任务微调方法}


#### 文本标注工具应用与各领域 NLP 任务标注实例 {#文本标注工具应用与各领域-nlp-任务标注实例}


#### 谷歌 BERT 系列经典模型架构及其应用分析 {#谷歌-bert-系列经典模型架构及其应用分析}


#### Openai 系列经典模型架构及其应用分析 {#openai-系列经典模型架构及其应用分析}


#### 文本生成模型 GPT 算法应用实例 {#文本生成模型-gpt-算法应用实例}


#### 作文与小说生成模型源码解读及其项目实战 {#作文与小说生成模型源码解读及其项目实战}


#### NER 命名实体识别项目流程及其应用实例 {#ner-命名实体识别项目流程及其应用实例}


#### 如何训练自己的标注数据,格式转换实例 {#如何训练自己的标注数据-格式转换实例}


#### 文本实体抽取与关系抽取算法与模型解读 {#文本实体抽取与关系抽取算法与模型解读}


#### 实体与关系抽取项目源码解读分析 {#实体与关系抽取项目源码解读分析}


### 自然语言处理通用框架-BERT 实战 {#自然语言处理通用框架-bert-实战}


#### 自然语言处理通用框架 BERT 原理解读 {#自然语言处理通用框架-bert-原理解读}


#### 谷歌开源项目 BERT 源码解读与应用实例 {#谷歌开源项目-bert-源码解读与应用实例}


#### 项目实战-基于 BERT 的中文情感分析实战 {#项目实战-基于-bert-的中文情感分析实战}


#### 项目实战-基于 BERT 的中文命名实体识别实战 {#项目实战-基于-bert-的中文命名实体识别实战}


#### 必备基知识点-word2vec 模型通俗解读 {#必备基知识点-word2vec-模型通俗解读}


#### 必备基础-掌握 Tensorflow 如何 {#必备基础-掌握-tensorflow-如何}


#### 必备基础知识点-RNN 网络架构与情感 {#必备基础知识点-rnn-网络架构与情感}


#### 医学糖尿病数据命名实体识别 {#医学糖尿病数据命名实体识别}


### 知识图谱实战系列 {#知识图谱实战系列}


#### 知识图谱介绍及其应用领域分析 {#知识图谱介绍及其应用领域分析}


#### 知识图谱涉及技术点分析 {#知识图谱涉及技术点分析}


#### Neo4j 数据库实战 {#neo4j-数据库实战}


#### 使用 python 操作 neo4j 实例 {#使用-python-操作-neo4j-实例}


#### 基于知识图谱的医药问答系统实战 {#基于知识图谱的医药问答系统实战}


#### 文本关系抽取实践 {#文本关系抽取实践}


#### 金融平台⻛控模型实践 {#金融平台-控模型实践}


#### 医学糖尿病数据命名实体识别 {#医学糖尿病数据命名实体识别}


### 语音识别实战系列 {#语音识别实战系列}


#### seq2seq 序列网络模型 {#seq2seq-序列网络模型}


#### LAS 模型语音识别实战 {#las-模型语音识别实战}


#### starganvc2 变声器论文原理解读 {#starganvc2-变声器论文原理解读}


#### starganvc2 变声器源码实战 {#starganvc2-变声器源码实战}


#### 语音分离 ConvTasnet 模型 {#语音分离-convtasnet-模型}


#### ConvTasnet 语音分离实战 {#convtasnet-语音分离实战}


#### 语音合成 tacotron 最新版实战 {#语音合成-tacotron-最新版实战}


### 推荐系统实战系列 {#推荐系统实战系列}


#### 推荐系统介绍及其应用 {#推荐系统介绍及其应用}


#### 协同过滤与矩阵分解 {#协同过滤与矩阵分解}


#### 音乐推荐系统实战 {#音乐推荐系统实战}


#### 知识图谱与 Neo4j 数据库实例 {#知识图谱与-neo4j-数据库实例}


#### 基于知识图谱的电影推荐实战 {#基于知识图谱的电影推荐实战}


#### 点击率估计 FM 与 DeepFM 算法 {#点击率估计-fm-与-deepfm-算法}


#### DeepFM 算法实战 {#deepfm-算法实战}


#### 推荐系统常用工具包演示 {#推荐系统常用工具包演示}


#### 基于文本数据的推荐实例 {#基于文本数据的推荐实例}


#### 基于统计分析的电影推荐 {#基于统计分析的电影推荐}


#### 基于相似度的酒店推荐系统 {#基于相似度的酒店推荐系统}
