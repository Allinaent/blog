+++
title = "在cloudfare中搭建hugo博客"
date = 2023-09-18T16:35:00+08:00
lastmod = 2024-06-06T16:04:29+08:00
tags = ["emacs", "hugo"]
categories = ["classic"]
draft = false
toc = true
image = "https://r2.guolongji.xyz/allinaent/2024/06/e1f639758ccc2e0aaa2d805109ecafa0.png"
+++

## hugo on cloudflare {#hugo-on-cloudflare}

我的博客就要开通了，codeberg pages + orgmode + hugo + stack theme，

Codeberg 是一个快速开源 GitHub 替代品，是一个面向开源社区的非营利性代码托管平台。它提供了与 GitHub 相似的功能，包括版本控制、问题跟踪和 Pull Request。不同的是，Codeberg 专注于保护用户的隐私和数据安全，并提供了更加开放和透明的运营方式。

以下是 Codeberg 的三个核心优势：

快速响应：Codeberg 采用了先进的硬件和软件配置，提供了更快的响应速度和更高的稳定性，让用户在开发过程中更加流畅。
数据安全：Codeberg 可以保护用户的隐私和数据安全，不会监视、分析或出售用户的数据。同时，Codeberg 也提供了备份和灾难恢复机制，确保用户数据的安全性和完整性。开源透明：Codeberg 的运营方式更加开放和透明，用户可以随时查看平台的运营状态和财务报告。Codeberg 通过社区捐赠和赞助等方式进行运营，保证了平台的独立和自主性。

体验了一下 codeberg，简直是完美。没有任何的博客能比得过 hugo 加 codeberg 了。当然了，博客都是记录一些精华的东西，可以不发布。但是手稿 draft 我也可以随便写。我终于可以成神了。我很喜欢 stack 的风格。曾经我以为
orgmode.org/worg 这个网站的风格适合做为博客，但是不好做。现在我更喜欢 stack 这个风格的博客，我一定要成功。未来是属于我的。

<https://sdl.moe/>

<https://github.com/CaiJimmy/hugo-theme-stack>

他用的是 cloudflare pages 和 gitlab。

完美主义者一点也不完美！最傻的就是完美主义者，没有人能第一种事就能把事情做好。关键是做了，而不是想了。这太重要了。

<https://sdl.moe/post/hexo-transfer-to-hugo/#%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%E4%B8%8E%E9%83%A8%E7%BD%B2>

开始干吧！！！！！！

1.  安装好 go，因为 hugo 的程序是依赖 go 的，源码安装需要安装 go。

2.  git clone git@github.com:SDLMoe/hugo.git

安装这个经过 mod 的版本。

1.  进入目录 go build，将 hugo cp 到/usr/local/bin/ 下面去即可。

2.  新建一个目录，肯定是个 git 项目，目录选一个好用一点的目录。就选 gh 目录下的 hugo-blog，作为目录吧。

即：hugo new site hugo-blog

1.  修改 config.yaml，这个配置文件。mv config.toml config.yaml

2.  加入 stack 这个主题

git submodule add git@github.com:CaiJimmy/hugo-theme-stack.git themes/hugo-theme-stack，注意不要加 themes，目录对不上。

1.  先放到本地仓库，以后有时间了再将本地主题的分支 glj 上传到自己的代码仓库。都放到 codeberg 或者是 Cloudflare

当中去。

1.  现在写一个文档吧，用 orgmode 写，并且上传到 codeberg 上面去。遇到了一个问题，就是 ox-hugo 导出的文章没有标题。之前以为是 hugo 配置文件的问题，后来发现是没有标题。

2.  emacs 是结合 yasnnipt 来实现写 org，用 ox-hugo 实现将 org 转化为 md，然后 commit 的动作使得博客的内容改变。

出了一点小问题，yaml 的配置如果不对就不能显示出来 toc。好在 github 上搜到了 hugo 的一些配置。参考着写就可以。挺好用的 hugo。只是像 emacs 一样。开始的时候没有 toc。

<https://github.com/J-shiro/J-shiro.github.io> ，参考这个实现，就有了。

现在的问题是我对 github 的 action，或者是 codeberg 的 CI 了解的不清楚。现在就是要了解这些方面的东西了，这些了解清楚了之后，我就能完成闭环，想清楚每一个环节比较好的作法。直到把有用的记录写到我的博客当中去。希望这个的时间不要太长，我等不及了。


### github Actions {#github-actions}

<https://www.ruanyifeng.com/blog/2019/09/getting-started-with-github-actions.html>

这篇文章写的很好。我理解了什么是 github Actions。就像 java 用 yml 一样。yaml 的一些优点和缺点：
<https://segmentfault.com/a/1190000039969764> ，yaml 并不是什么高级的东西，反而它是比较低级的，好处是符号少，能满足一定的需求。


### codeberg 的 CI {#codeberg-的-ci}

<https://codeberg.org/Codeberg-CI/request-access>
<https://codeberg.org/Codeberg-e.V./requests>


### gitee 也是有 pages {#gitee-也是有-pages}

文档是中文的，这一点比较爽。但是啥呢。


### 自己的随笔也保留 {#自己的随笔也保留}

自己乱七八糟的笔记和整理好的博客，显然，是要分开的。最好的做法是笔记也保留在私有仓库，博客也保留在私有仓库，然后通过 CI，只发布 blog 指定目录到另一个项目当中。现在我对项目的组织结构还有没有什么经验。一定要把关系理清楚，时机成熟了之后再开始做。否则不能长远。后面一旦换了机器，或者有些什么其它的问题也是解决不了的。

好的好的。现在查看一下目录的情况吧。

```bash
uos@guolongji:~/gh/hugo-blog$ tree -L 2
.
├── archetypes
│   └── default.md
├── assets
│   └── jsconfig.json
├── config.yaml
├── config.yaml~
├── content
│   └── post
├── data
├── hugo-theme-stack
│   ├── archetypes
│   ├── assets
│   ├── config.yaml
│   ├── data
│   ├── debug.sh
│   ├── exampleSite
│   ├── go.mod
│   ├── i18n
│   ├── images
│   ├── layouts
│   ├── LICENSE
│   ├── netlify.toml
│   ├── README.md
│   └── theme.toml
├── layouts
├── org
│   └── 2023
├── public
│   ├── 404.html
│   ├── categories
│   ├── img
│   ├── index.html
│   ├── index.xml
│   ├── page
│   ├── post
│   ├── scss
│   ├── sitemap.xml
│   ├── tags
│   └── ts
├── resources
│   └── _gen
├── static
└── themes
    └── hugo-theme-stack
```

我和其它的 hugo 用户不同的是我用了 emacs。看了这一篇文章：
<https://juejin.cn/post/7120474763910676488> ，现在明白了，现在核心就是这个私有的仓库。还有几个有问题的点需要思考。

首先，用 github 的 Action 我用的是经过 mod 的 hugo 版本。经过 mod 的 hugo 版本应该如何让 github 调用编译命令呢，这是一个值得思考的问题。

<https://www.lixueduan.com/posts/blog/01-github-action-deploy-hugo/>
这篇文章写清楚了 hugo 的 Action 的原理。

什么是高级工程师呢？<https://www.lixueduan.com/posts/> ，看看别人大神搞的东西，发现人的落后真的是非常的大的。你还在研究的事情，可能是别人早就已经用的很明白的东西了。不过也无可厚非，毕境，一个人的精力是有限的。祈祷吧。今天已经够累的了。

可不可以理解，智商高的人不是不累，而是承受能力强呢？是的，他们不是说做什么事情都轻松，只是经常性的全力以赴而已，所以不要相信别人能很容易的就达到什么样子的成就。就算是买彩票中奖，也是因为人家付出来买的动作，种豆得瓜。普通人会没事买一张彩票吗？

垃圾太多了。说我兴趣广泛，和真正的博士相比，我还是懂得太少了。就算学会了，其实也没有什么大用处。又回顾了一下：
<https://sdl.moe/post/hexo-transfer-to-hugo/> ，发现用 cloudflare 挺好的。密码必须最后加一个英文感叹号。要什么特殊字符，烦人。

看一下 cloudflare 的文档，官方文档不说写的多好，起码是有用的。cloudflare 加 gitlab，这两个组合有人用。那就应该挺好用的。

越做就越接近于成功，行动胜过千言。虽然说话也很重要，但是对于一个人自己的进步，还是行动起到真正的作用。人的工作是实践出来的，而不是说出来的。

astro，也是一个博客框架： <https://docs.astro.build/zh-cn/guides/deploy/cloudflare/>

没必要了解那么多不相关的东西。我还是需要明确目标，分散精力有时并不太好。想弄漂亮的静态博客还是很容易的。

编码能力强只是写的代码多而已，也写的很快，所以不要觉得万事很难。

cloudflare 的 CDN，简直是写自己博客的不二选择。参见另一个使用原生 stack 主题的大佬的博客：
<https://tutuis.me/deploy-blog-to-cloudflare-pages/> ，每个月的构建时长是 500 分钟，也就是 8 个小时，这么长的时间，你能用光，那是这辈子都不可能的一件事。

为了绐我的 emacs 找个好一点的展示的家，我真的是不容易啊。选一个漂亮的博客，而后再用 latex 或者 orgmode 来做笔记。<https://shaohanyun.top/posts/tech/transformer/>

今天把这个弄完吧。

我的选型是 cloudfare + gitlab。

现在步骤到了将 hugo 的项目上传到 gitlab 当中，这个项目的上传很有学问，要小心要细致。我有一个干净的 gitlab 帐号，不要浪费了这么干净的一个号。gitlab 设置了和 github 一样的一个密码。

我要编辑一下.gitignore，不要管理 **\*~** 这种 emacs 生成的文件。不要管理最外面的 org、content 两个文件夹。这样应该就行了吧。哪里有那么复杂，只是我过于谨慎了而已。记住 gitignore 不要加点。因为这些语法的细节，会让你的项目变的非常地好用，而不是一团乱麻。计算机这个工作是十分看重经验的一项工作。大多数时候，脑子笨一点都是无所谓的，有所谓的只是如何把问题解决掉。另外 resources 这个文件夹下面的东西也是没有什么用处的。也加到 gitignore 当中就可以了。

这个步骤是这个样子的。公有项目只显示发布的内容。而私有的项目只存 md 和 org 的源文件。而我用 emacs 只修改的是
org 的源文件，md 的源文件是通过 emacs 的命令来生成的。

got it！ 就是这个样子，ok。现在没有什么问题了吧。

好的，现在这个思路没有问题了。还有就是你把头像什么的都改一下，别弄一个半成品的第一次的提交，让自己费力多搞几次，那就很烦。第一步选一个头像。好看的头像，或者就用我的 QQ 头像就可以了。

convert lbxx.jpg -resize 150x150 avatar.jpg

直接重新运行就可以了，牛逼。这款博客的主题广为流行，真的是很好用的。

git remote add origin <https://gitlab.com/Allinaent/blog>
git branch -M main
git push -u origin main

git config --global url."<https://oauth2:glpat-wkuWyshujRmsmGxEgFxL@gitlab.com/Allinaent/blog%22.insteadof>
"<https://gitlab.com/Allinaent/blog>"

git push --set-upstream origin main，这个命令成功了。

多次上传会导致错误，需要手动修改~/.gitconfig 文件来完成更新。

一定要先 initial commit 一次。

现在上传成功了。好吧，cloudflare 不能用 submodule，这个真的是有一点烦。那还有什么呢？现在我的博客一篇文章也没得，需要上传一些文章。

gitlab 的 CI，持续集成。看这篇文章：<https://www.jianshu.com/p/2a5c77b4d683> ，这个人用的是 github 做的，应该和
gitlab 是差不多的。CI 没什么神秘的：<https://zhuanlan.zhihu.com/p/282958129>， 你可以看出来，CI 只是一些运维脚本的组合，ymal 只是把这些元素做一些规范化的定义而已。

<https://m.hswh.org.cn/wzzx/llyd/ls/2023-08-09/83253.html> ，树的影，人的名。毛主席还见过李政道，并且创建了中科大的少年班。这是一段佳话。每一个在科学上有成就的人都是那么的强壮，现在杨振宁和李政道都一百岁左右了。然而还都活着。人与人相比，差距真的是太大了。想从人修炼成神真的是要付出超出常人的努力。还有不要对自己失去信心，我就算是什么也做不成了，30 多岁了，可是做些学术上的研究，总归不是什么不良爱好。迟早有一天我会因为我自己的坚持让后人受益。

我的博客发布流程，大概是这样的。本地写 org 博客。一键生成 md 的博客文件。org 的内容要不要建立一个项目呢？要！你的博客要让别人不方便复制吗？可能是有必要的。

<https://www.xheldon.com/tech/the-using-of-github-pages.html> ，这篇文章的思路和我自己的思路是完全一致的。甚至如果是 github 的 pages 你都不需要修改，直接用人家的方案就可以了。但是他用的是 jekyll，稍微有一些区别。

更高层次的认知，并能付诸于实践，这太重要了。当然，其实我写的博客应该不会有什么人来看，一是别人看一懂，二是看的懂的可能也不屑于看吧。

用 orgmode 来写 hugo 博客。

hugo 的目录组织结构，按时间来组织是最合适的。没有问题。在 gitlab 中创建一个私有的子项目。

<https://www.knightli.com/2022/03/25/hugo-md-%E7%9B%AE%E5%BD%95%E7%BB%84%E7%BB%87/>

git subtree 这个和 git 的 submodule 不太一样，但是十分的类似，可以看看下面的文章：

<https://tangxusc.github.io/blog/2019/03/git-subtree-%E6%95%99%E7%A8%8B/>

我要怎么做呢？

gitlab 和 github 的 ci 本质都是一种编程了。首先要认识这个过程是什么，然后用语法写出来。github actions 稍微有一点博大精深了啊。github 的 action market 上搜一下，看是不是有别人写好的 ci 工具？大概是有的。

本质就是用到了 git push -f，可以在不同的项目之间做推送。github 还是 gitlab 都是暂时使用的一个虚拟机，直的是挺神奇的。也不知道具体的实现方式。

选 gitlab 是一个非常好的选择，因为我在手机上也可以访问 gitlab 个人的主页，关键是一点也不卡，不用翻墙也能访问。这非常的棒。以后就用这个 gitlab 了，简直是最棒的一个选择。

现在我理解了完全使用私有的 md 可能很麻烦，而且得不偿失。因为我写的东西大多数情况也不是什么机密的内容。而且本来就是为了分享的。

还是回归到本质，用：
<https://sdl.moe/post/hexo-transfer-to-hugo/#%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%E4%B8%8E%E9%83%A8%E7%BD%B2>

这个来做博客吧。cloudfare 是最好的选择，是否需要隐私。经过我慎重的考虑，不是一个关键的问题。今天差不多可以弄完，Ok，你又要完成一个重要的工作了，加油。人工智能时代，在博客中记录学习过程，终于可以走上正轨了。

写完之后发现没有滚动条，怎么回事？在配置文件里面把代码有行号的配置绐注释掉就好了。墨语的博客优化的很好看：

<https://moyu.ee/>

<https://moyu.ee/p/hugo-stack/#%E6%BB%9A%E5%8A%A8%E6%9D%A1%E7%BE%8E%E5%8C%96>

这个人页面优化的也不错：

<https://xrg.fj.cn/>

原来 hugo 和 hugo 的各种主题早就广为流传了。之前还想自己写一个博客框架或都完全把博客搞成一个自己的项目。现在感觉不是特别地有必要了。人生也有涯，没用人是无涯子。give up and turn to learn AI！


## 修改了 katex.min.css 和 js 的 cdn {#修改了-katex-dot-min-dot-css-和-js-的-cdn}

e themes/hugo-theme-stack/data/external.yaml，选用了七牛云的 CDN。

现在网页显示公式基本上是秒出了，这就很棒。以后多理解多学习吧。


## hugo stack theme 不支持灯箱 {#hugo-stack-theme-不支持灯箱}

广大的开源用户是有实力的，别人已经有了很好的方案，我可以直接抄过来。参考下面这个，效果是真的好用啊。

<https://blog.l3zc.com/2023/10/theme-stack-tweaks/>

另外这个大佬用到的评论系统是：Twikoo ，这个有时间也试试吧。

创建./layouts/partials/comments/provider/twikoo.html，修改文件第一行：

&lt;script src="//lib.baomitu.com/twikoo/1.6.21/twikoo.all.min.js"&gt;&lt;/script&gt;

原主题的 Twikoo 版本是 1.16.115，这里也顺便升级成匹配我部署的后端版本 1.16.21。

其他的评论系统也以此类推，折腾的同学有很多，我应该也会帮助到一些人吧。时间长了就逐渐理解这些开源的项目了。


### 灯箱最终用了 fancybox {#灯箱最终用了-fancybox}

参考的这篇文章：

<http://www.heiyunw.com/post/2257.html>

git show HEAD themes/hugo-theme-stack/

```diff
diff --git a/themes/hugo-theme-stack/layouts/partials/footer/custom.html b/themes/hugo-theme-stack/layouts/partials/footer/custom.html
index c332b98..2c707d8 100644
--- a/themes/hugo-theme-stack/layouts/partials/footer/custom.html
+++ b/themes/hugo-theme-stack/layouts/partials/footer/custom.html
@@ -1,2 +1,10 @@
-<script src="/imgzoom/zoom-vanilla.min.js"></script>
-<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_SVG"></script> -->
+<!-- <script src="https://lib.baomitu.com/jquery/3.6.0/jquery.min.js"></script> -->
+<!-- <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0.12/dist/fancybox.umd.js"></script> -->
+<script src="https://testingcf.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js"></script>
+<script src="https://testingcf.jsdelivr.net/npm/@fancyapps/ui@4.0.12/dist/fancybox.umd.js"></script>
+<script>
+  // 获取文章中的img 标签 不包括封面
+  $('.main img').not(".cover").each(function () {
+      //添加 data-fancybox="gallery"
+      $(this).attr("data-fancybox","gallery"); })
+</script>
diff --git a/themes/hugo-theme-stack/layouts/partials/head/custom.html b/themes/hugo-theme-stack/layouts/partials/head/custom.html
index 48144ec..b730512 100644
--- a/themes/hugo-theme-stack/layouts/partials/head/custom.html
+++ b/themes/hugo-theme-stack/layouts/partials/head/custom.html
@@ -1 +1,3 @@
-<link href="/imgzoom/zoom.css" rel="stylesheet">
+<!--<link rel="stylesheet" href="https://lib.baomitu.com/fancybox/3.5.7/jquery.fancybox.min.css">-->
+<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0.12/dist/fancybox.css">-->
+<link rel="stylesheet" href="https://testingcf.jsdelivr.net/npm/@fancyapps/ui@4.0.12/dist/fancybox.css">
```

博客越来越实用，现在可以尽情写有技术文档了。
