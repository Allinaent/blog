+++
title = "什么是 initramfs"
date = 2024-11-27T16:32:00+08:00
lastmod = 2024-11-27T16:57:15+08:00
categories = ["kernel"]
draft = false
toc = true
image = "https://r2.guolongji.xyz/allinaent/2024/06/92a1feeab471b12646b9c76edccc1546.jpg"
+++

<https://www.bilibili.com/video/BV12G4y1z7p8/?spm_id_from=333.337.search-card.all.click&vd_source=702481d9edf811d1f18266d09c074e00>

对于 bootloader 和 initramfs 这两个没有什么神秘的。其实要学会的只是调试过程中的一些常用的命令。

{{< figure src="https://r2.guolongji.xyz/allinaent/2024/11/229e0cb26036903cd10b0f9ad62acc4c.png" >}}

{{< figure src="https://r2.guolongji.xyz/allinaent/2024/11/6e9fcb713e09735ab8b151e12978ba81.png" >}}

{{< figure src="https://r2.guolongji.xyz/allinaent/2024/11/528434b53eec42ffe4de8390fdaf4f57.png" >}}

{{< figure src="https://r2.guolongji.xyz/allinaent/2024/11/e1de6c106a353a964243d998910cc8d6.png" >}}

{{< figure src="https://r2.guolongji.xyz/allinaent/2024/11/1e78cfb845f3e2ba72b3bd105035e900.png" >}}

{{< figure src="https://r2.guolongji.xyz/allinaent/2024/11/597fc0f71c2ba1114ea0f440e4fca411.png" >}}

{{< figure src="https://r2.guolongji.xyz/allinaent/2024/11/9c25870446eb3eaf1c9bb26cbf4d3cf1.png" >}}

{{< figure src="https://r2.guolongji.xyz/allinaent/2024/11/663b3ccdd9177d452631e7d1b21ea684.png" >}}

{{< figure src="https://r2.guolongji.xyz/allinaent/2024/11/ef430376edefb0851d34838511ce35db.png" >}}
