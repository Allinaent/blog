+++
title = "研究生英语单词 2000 词——1"
date = 2024-05-07T14:40:00+08:00
lastmod = 2024-06-04T09:56:05+08:00
categories = ["exam"]
draft = false
toc = true
image = "https://r2.guolongji.xyz/allinaent/2024/06/bc99472328e3d7d6e30f6e58d299cec8.jpg"
+++

考研英语单词 7000 个。背单词多多益善，我计划分三篇博客将这 7000 个单词中我不会的总结出来（一词大概是 2000 词背完后总结），方便没事的时候多看两眼。


## 文艺复兴基金创始人——詹姆斯西蒙斯 {#文艺复兴基金创始人-詹姆斯西蒙斯}

“我不是世上最机敏的人，要是参加数学奥林匹克竞赛，我的表现也不会特别好。可我喜欢琢磨，在心里琢磨事，也就是反反复复地思考某些事。事实证明，那是种很棒的方法。”

西蒙斯是一个天才。我显然不是什么天才。但是我从他的话里学到了。我可以不因为自己的愚蠢而自悲。

年少之人要充分利用时间，学一些屠龙技。而世上难以修炼却开源的武功密籍就是数学和理论物理这些东西。如果始终保持兴趣，普通人也可能成为一个科研民工，再往后，下一代，发挥一代更比一代强的科研精神。也许就真的能为科学做出点贡献。

2000 个词的收藏的生词有 1000 个，记录如下：

单词背四个月可以背完，然后再学习一些新的东西。估计需要新背 4000 个词，背完了之后做题和专业的训练。一定要练到位。我最担心的是数学。数学是我喜欢的，但是我现在在数学上已经远远落后了。是我的一个缺陷，从比较长的长处变成了比较短的短处，所以要拣起来。学到一个让自己满意的程度。


## 开始 {#开始}

0510 ：

simultaneous ：同步的，同时发生的

rein ：缰绳；控制；保护带

reign ：君主任期

rehearse ：排练，排演

regime ：政权，政体，组织方法

regarding ：关于，至于

pneumonia ：肺炎

plentiful ：丰富的，充足的

pledge ：保证，誓言，抵压品

nominate ：提名；推荐，任命，指定

nominal ：名义上的，很小的

nightmare ：梦魇

mechanical ：机动的，机械驱动的

mechanic ：机械师，机械修理工

leather ：皮革，皮衣

lease ：租约，租契

leap ：跳，跳跃

league ：联赛，联盟

leaflet ：传单；小册子

knot ：绳结；郁结

knob ：旋钮；球形把手

kneel ：跪下，跪着

infrastructure ：基础建设，基础设施

infrared ：红外线的

inflict ：使遭受

inflation ：通货膨胀，充气膨胀

infectious ：传染性的，感染的；有感染力的

analytical ：分析的，分析性的

infect ：传染，使感染

infant ：婴儿，学童

inertia ：惯性，缺乏活力，惰性

analogue ：模拟的，指针式的

analogy ：类比的，相似的

0509:

besides 除什么之外；还

amplify ：放大

beneath 在下面，往下；配不上

heritage 遗产，传统

herd 芸芸众生，兽群，牧群

herb 草药

herald 是什么的前兆；宣布

henceforth 从今以后，今后，从现在起

hemisphere 半球，半脑

helicopter 直升机

heir 继承人，接班人

heel 脚后跟，后跟

hedge 树篱；防止损失的手段

gown 女礼服

gossip 流言蜚语

gergeous 非常漂亮的

enclose 把什么围起来

deputy 副手，代理人；议员

deplore 强烈遣责

deplete 大量消耗

depict 描绘；描写，刻画

departure 离开，出发，违返

depart 离开

clause 从句，条款

clasp 握紧，抓紧

clash 打斗，打架，冲突

clarity 清晰；清楚

clarify 阐明，澄清，清楚

clap 拍手；轻拍

bench 长凳，长椅

beloved 钟爱的，挚爱的，被深爱的

belly 肚子，腹部

amidst 在什么什么过程中

0508

amid 在什么什么过程中，被什么什么环绕

amiable 和蔼的，友好的

ambassador 大使，使节

wax 蜡，蜂蜡，石蜡

waterproof 防水的，不透水的

voyage 航行，航海，航天

volcano 火山

volatile 易变的，不稳定的，易挥发的

vocation 工作，职业

vocal 嗓音的，发音的，直言不诲的

unite 使团结；全联合，使统一

virgin 处女，处男，新手

violet 紫罗兰

unity 团结，统一

thrust 猛推，刺，戳

threshold 门槛，起始点，监界点

sightseeing 游览，观光

sigh 叹气

siege 围困；封锁，包围

sideways 侧着

gracious 和蔼的，有礼貌的

graceful 优美的，优雅的，得体的

grace 优美，优雅

civilization 文明，文化

civilize 教化，使开化，使文明

civilian 平民，老百姓

civil 国民的，民用的

0507 ：

queer ：奇怪的，异常的，古怪的

quiver ：颤抖、哆嗦

quiz ：小测试，智力竞赛

quota ：定额，限额，指标

recall ：召回

recollect ：记起，回忆起

reconcile ：使和好，妥协

recur ：（不好的事）再次发生

recreation ：娱乐，消遣

recycle ：循环利用

rectify ：矫正，纠正，改正

redundant ：多余的、累赘的、被裁员的

reed ：芦苇

reel ：卷轴，一卷；vi 踉跄，感到震惊

sheer ：陡峭的，完全的，纯粹的

shepherd ：牧羊人，护送

shrewd ：精明的，敏锐的，有眼光的，精于盘算的

shrink ：收缩，退缩

shutter ：百页窗，快门

shuttle ：航班，班车、火车；梭，梭子

sheet ：床单

theft ：偷窃，盗窃，盗窃罪

thermal ：热量的，保暖的

thermometer ：温度计

thesis ：学术论文

thigh ：大腿，股

thorn ：刺、荆棘

viable ：可行的，可实施的

vibrate ：振动、颤动

vicinity ：周围地区

voilate ：违反，违背

being ：存在、生物、身心

allege ：断言、宣称；指控

allegiance ：忠诚、拥护

alleviate ：减轻、缓解

alliance ：联盟

allowance ：津贴，补贴

alloy ：合金，把什么铸成合金

ally ：同盟国，盟友

alongside ：在什么旁，与什么什么一起

denounce ：遗责，指责，告发

dense ：密集的

density ：密度

fellow ：小伙子

fellowship ：友谊，交情

feminine ：女性的，女性化的

fence ：栅栏，障碍物

ferry ：渡船；渡轮，摆渡

feudal ：封建的

fever ：发烧，狂热，热病

heave ：举起

induce ：引诱、引起、催产

indulge ：沉迷，使满足、纵容，迁就

kin ：家属，亲戚

kit ：成套工具

mat ：小地毯，垫子

mayor ：镇长，市长、郡长

nest ：鸟巢，巢穴

omit ：删除，忽略

opaque ：不透明的，难懂的

neutral ：中立的，持平的

opt ：选择，挑选

optical ：视力的，光学的

plaster ：灰泥，膏药，创可贴

plastic ：塑料，信用卡

plateau ：高原，稳定期

refine ：提炼

refrain ：克制，避免，陈词滥调

refuge ：避难

refugee ：避难者

refund ：退款，退还

refusal ：拒绝 n

circuit ：环行，环行线路，电路，巡回

circular ：圆形的，绕圈的

circulate ：循环，流通，往来应酬

0506

eligible 有资格的，合意的

elevator 电梯，升降机

elevate 提拔；抬起；提高

elect 选择

delight 高兴，愉快

pint 品脱

pinch 掐，捍；捍住；行窃，逮捕

chunk 厚块；大块；大量，语块

chronic 慢性的；长期的；积习难改的

chorus 副歌；合唱曲；合唱团

chill 寒冷；凉意；害怕的感觉

chief 首领

chew 咀嚼，嚼碎

cheque 支票

aisle 走道，过道，走廊

texture 质感；口感；韵味

terror 恐怖；恐惧

territory 领土；地区；领域

terrific 极好的，很校友会上的，极大的

shed 棚屋；厂房；工棚

shave 剃；刮；削

shatter 破碎，希望破灭

shallow 浅的，肤浅的；弱的

shake 奶昔；摇动；颤抖

0503

bond 纽带；债券；保释金；羁绊，键
