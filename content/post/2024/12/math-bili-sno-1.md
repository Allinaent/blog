+++
title = "b 站上的数学好课"
date = 2024-12-12T19:35:00+08:00
lastmod = 2024-12-13T16:32:04+08:00
categories = ["math"]
draft = false
toc = true
image = "https://r2.guolongji.xyz/allinaent/2024/06/3347f176357f4145e1d75ccd268ee8d8.jpg"
+++

## 集成低分辨率的视频 {#集成低分辨率的视频}

<https://www.bilibili.com/video/BV164411o7Qo>

将视频用 shortcode 集成一下。清晰度只有 360p ，不过没关系，可以跳转，这个的目的只是为了强调一下视频。

{{< bilibili "BV164411o7Qo" "92" >}}


## 还是做笔记 {#还是做笔记}

重点是多元函数求导法则。

中值定理。

目标就是过线而已。
