+++
title = "kdump 的总结和介绍"
date = 2024-01-23T17:29:00+08:00
lastmod = 2024-06-06T13:19:47+08:00
categories = ["kernel"]
draft = false
toc = true
image = "https://r2.guolongji.xyz/allinaent/2024/06/92a1feeab471b12646b9c76edccc1546.jpg"
+++

kdump 有一篇很好的分享：

[file:/Kexec_Kdump_Impl_Appl_v2.pdf](/ox-hugo/Kexec_Kdump_Impl_Appl_v2.pdf)
