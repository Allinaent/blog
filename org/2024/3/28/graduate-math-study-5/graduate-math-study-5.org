#+OPTIONS: author:nil ^:{}
#+HUGO_BASE_DIR: ../../../../../
#+HUGO_SECTION: post/2024/03
#+HUGO_CUSTOM_FRONT_MATTER: :toc true
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_DRAFT: false
#+DATE: [2024-03-28 四 09:39]
#+TITLE: 第五章——常微分方程
#+HUGO_TAGS:
#+HUGO_CATEGORIES: exam
#+HUGO_CUSTOM_FRONT_MATTER: :image https://r2.guolongji.xyz/allinaent/2024/06/fd73d15b3d78f6756f87c0b048d163a2.jpg

未知函数是一元函数的微分方程称作常微分方程。此处的“常”是 Ordinary 表示平常。
数学于人，是一些深刻的逻辑和概念，最先是概念。有了概念才有了基础，才能展开分析。

齐次的英文单词是 homogeneous ，原义是“同质的”

* 常微分方程

- 微分方程的基本概念
- 常见的几类一阶方程及解法
 + 可分离变量的方程
 + 齐次方程
 + 线性方程
- 可降阶的高阶微分方程
 + $y^{(n)}=f(x)$ 型微分方程
 + $y''=f(x, y')$ 型的微分方程（不显含 y ）
 + $y''=f(y, y')$ 型的微分方程（不显含 x ）
- 高阶线性方程
 + 线性方程解的结构
  - 齐次方程解的结构
  - 非齐次方程解的结构
  - 线性方程解的叠加原理
 + 线性常系数微分方程求解（线性是指不含指数，常是指导数表达式的系数为常，齐次是指不含常数项）
  - 线性常系数齐次方程求解（对 x 是齐次的，x 的系数为 0）
  - 线性常系数非齐次方程求解（等式右边为 $(f(x)$ ）

** 例题分析

- 微分方程求解
 + 一阶方程求解
 + 二阶可降阶方程求解

- 微分方程综合题
- 微分方程的应用
