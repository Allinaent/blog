#+OPTIONS: author:nil ^:{}
#+HUGO_BASE_DIR: ../../../../../
#+HUGO_SECTION: post/2024/03
#+HUGO_CUSTOM_FRONT_MATTER: :toc true
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_DRAFT: false
#+DATE: [2024-03-28 四 22:58]
#+TITLE: 线代第二章——矩阵
#+HUGO_TAGS:
#+HUGO_CATEGORIES: exam
#+HUGO_CUSTOM_FRONT_MATTER: :image https://r2.guolongji.xyz/allinaent/2024/06/e0dd48553ae47b1f7a78c4e75f4f0763.jpg

* 行列式与矩阵的区别与联系

- 区别

 + 矩阵是一个表格，行数和列数可以不一样；而行列式是一个代数和，当元素是数时，它是数，且行数必须等于列数。

 + 两个矩阵相等是指对应元素都相等；两个行列式相等不要求对应元素都相等，甚至阶数也可以不一样，只要最后代数
和的结果一样就行了。两矩阵相加是将各对应元素相加；两行列式相加，是将运算结果相加。

 + 数乘矩阵是指该数乘以矩阵的每一个元素；而数乘行列式，只能用此数乘行列式的某一行或列，提公因数也是这样的。

 + 矩阵经初等变换，其秩不变；行列式经初等变换，其值可能改变。

- 联系
 + 当矩阵 $A$ 和 $B$ 是同阶方阵时，有 $|A\cdot B|=|A|\cdot|B|$ ，公式很简洁，要理解其中含义。


* 矩阵的概念及运算

- 概念（矩阵就是一个表格）
- 矩阵的运算
- 矩阵的运算规则
 + 加法（同型矩阵可以相加）
 + 数乘（乘以矩阵当中的每一个元素）
 + 乘法（矩阵乘法，行与列各自相乘并相加，特别的 $A^{k}$ A 是方阵就是做 k 次自乘运算）
   $(AB)C=A(BC)$

   $A(B+C)=AB+AC$

   $(B+C)A=BA+CA$

   总的来说就是符和交换律但是不符和结合律。

 + 转置（行与列互换）
   $(A+B)^{T}=A^{T}+B^{T}$

   $(kA)^{T}=kA^{T}$

   $(AB)^{T}=B^{T}A^{T}$ （这个公式有点意思）

   $(A^{T})^{T}=A$

 + 方阵的幂
   $(A^{k})^{l}=A^{kl}$ , $A^{k}A^{l}=A^{k+l}$

   $(A+B)^{2} = A^{2} + AB + BA + B^{2} \neq A_{2}+2AB+B^{2}$

   $(A+B)(A-B) = A^{2} - AB + BA - B^{2} \neq A^{2}-B^{2}$

- 特殊矩阵
 + 单位阵
 + 数量阵 $kE$
 + 对角阵（非对角元素都是 0)
 + 反对称阵
 + 正交阵（$A^{T}A=AA^{T}=E$ 的矩阵称为正交阵，亦 $A^{T}=A^{-1}$ ）
 + 初等矩阵：单位矩阵经过一次初等变换所得的矩阵。这个不熟
 + 行阶梯矩阵（底部为零行，主元所在列下面的元素都是 0 ）
 + 行最简矩阵（所有主元为 1 的行阶梯矩阵）

* 伴随矩阵、可逆矩阵

- 概念
 + 伴随矩阵（由代数余子式构成的矩阵）
 + 逆矩阵（ $AB=BA=E$ ，则 A 和 B 互为逆矩阵）

- 伴随矩阵重要公式

  + $AA^{* }=A^{* }A=|A|E$

  + $(A^{* })^{-1}=(A^{-1})^{* }=\frac{1}{|A|}A$  条件为 $(|A| \neq 0)$

  + $(A^{* })^{T}=(A^{T})^{* }$

  + $(kA)^{* f}=k^{n-1}A^{* }$ 这个不知道是怎么来的。

  + $|A^{* }|=|A|^{n-1}$ ； $(A^{* })^{* }=|A|^{n-2}A$ 在 $(n \geq 2)$

  + 伴随阵的秩与原阵秩的关系
  \[
  r(A^{*}) =\begin{cases}
  &n, r(A)=n, \\
  &1, r(A)=n-1, \\
  &0, r(A)<n-1 \end{cases}
  \]



- n 阶矩阵 A 可逆的充分必要条件
 + 存在 n 阶矩阵 $B$ ，使 $AB=E$ （或 $BA=E$ ）
 + $|A| \neq 0$ ，或秩 $r(A)=n$ ，或 $A$ 的列（行）向量线性无关。
 + 齐次方程组 $Ax=0$ 只有零解
 + $\forall b$ ，非齐次线性方程组 $Ax=b$ 总有唯一解
 + 矩阵 $A$ 的特征值全不为 0 。

- 逆矩阵的运算性质
 + 若 $k \neq 0$ ，则 $(kA)^{-1}=\frac{1}{k}A^{-1}$
 + 若 $A,B$ 可逆，则 $(AB)^{-1}=B^{-1}A^{-1}$ 特别地 $(A^{2})^{-1}=(A^{-1})^{2}$;
 + 若 $A^{T}$ 可逆，则 $(A^{T})^{-1}=(A^{-1})^{T}$
 + $(A^{-1})^{-1}=A$
 + $|A^{-1}|=\frac{1}{|A|}$
 + 即使 $A,B$ 和 $A+B$ 都可逆，一般地 $(A+B)^{-1} \neq A^{-1}+B^{-1}$

- 求逆矩阵的方法

 + 公式法 $A^{-1}=\frac{1}{|A|}A^{* }$
 + 初等变换法 $(A|E)\xrightarrow{\text{elementary row transformation}}(E|A^{-1})$
 + 若 $AB=E$ 或 $BA=E$ ，则 $A^{-1}=B$
 + 分块矩阵的公式
  \[
   \left [ \begin{matrix}
   B & O \\
   O & C
   \end{matrix} \right ]^{-1}=\left [ \begin{matrix}
   B^{-1} & O \\
   O & C^{-1}
   \end{matrix} \right ]
   \]

  \[ \left [ \begin{matrix}
   O & B \\
   C & O
   \end{matrix} \right ]^{-1}=\left [ \begin{matrix}
   O & C^{-1} \\
   B^{-1} & O
   \end{matrix} \right ] \]

* 初等变换、初等矩阵

- 定义
 - 初等变换（倍乘，互换、倍加）
 - 初等矩阵：E 经过一次初等变换形成的矩阵
  + 倍乘初等矩阵
  + 互换初等矩阵
  + 倍加初等矩阵
 - 等价矩阵：经过有限次初等变换可成为一样的矩阵称之为初等矩阵，记作 $A\cong B$
   若 \[ A\cong \left [ \begin{matrix} E_{r} & O \\ O & O\end{matrix} \right ] \] ，则后者称为 $A$ 的等价标准形。

- 初等矩阵与初等变换的性质
 + 初等矩阵的转置仍是初等矩阵
 + 初等矩阵均是可逆阵，且其逆阵仍是同一类型的初等矩阵
 + 用初等矩阵左乘（右乘）A ，相当于对 A 作相应的初等行（列）变换
 + 当 A 是可逆阵时，则 A 可作一系列初等行变换化成单位阵，即存在初等矩阵 $P_{1},P_{2}, ... ,P_{N}$ ，使得
   $P_{N}...P_{2}P_{1}A=E$

* 矩阵的秩

- 概念：北京理工大学课本的概念很清晰，矩阵 $A$ 经初等变换化成的阶要梯形矩阵中非零行的数目称为矩阵
  $A$ 的秩，记为 $r(A)$ ；经初等变换矩阵的秩不变；若 $A$ 可逆，则 $r(AB)=r(B),r(BA)=r(B)$ 。

- 矩阵秩的公式
 + $r(A)=r(A^{T})$ ; $r(A^{T}A)=r(A)$ ，一个矩阵的转置左乘它自己，秩大小不发生变化。

 + $k \neq 0, r(kA)=r(A)$ ； $r(A+B) \leq r(A) + r(B)$ ； $r(AB) \leq \min (r(A)+r(B))$

 + $r(AB) \leq min(r(A), r(B))$

 + 若 $A$ 可逆，则 $r(AB)=r(B)$ ， $r(BA)=r(B)$

 + 若 $A$ 是 $m\times n$ 矩阵，$B$ 是 $n\times s$ 矩阵，$AB=O$ ，则 $r(A)+r(B) \leq n$

 + 分块矩阵 $r\left (\begin{array}{cc}A & O \\O & B \\ \end{array}\right)=r(A)+r(B)$

* 分块矩阵

- 概念
- 运算
  + 分块加法（没错，就是你想的那样）
  + 分块乘法（没错，就是你想的那样，每个小块都可以看做是数字）
  + 分块转置 $\left [ \begin{matrix}A & B \\C & D\end{matrix} \right ]^{T}= \left [ \begin{matrix}A^{T} & C^{T} \\B^{T} & D^{T}\end{matrix} \right ]$
  + 对角 n 次方 $\left [ \begin{matrix}B & O \\O & C\end{matrix} \right ]^{n}= \left [ \begin{matrix}B^{n} & O \\O & C^{n}\end{matrix} \right ]$

  + 对角可逆 $\left[ \begin{matrix}B & O \\O & C\end{matrix} \right ]= \left [ \begin{matrix}B^{-1} & O \\O &
    C^{-1}\end{matrix} \right ]$
    $\left [ \begin{matrix}O & B \\C & O\end{matrix} \right ]^{-1}= \left [ \begin{matrix}O & C^{-1} \\B^{-1}
    & O\end{matrix} \right ]$
  + 从解的性质分析 $AB=O$ ，A 是 $m\times n$ ，B 是 $n\times s$ 矩阵（齐次式）
  + 若 $AB=C$ ，则矩阵 $AB$ 可由 $B$ 的行向量线性表出；可由 $A$ 的列向量线性表出。


* 例题分析

暂无，线性代数学起来真的抽象加头痛。光是概念、运算、公式就一大堆。
