#+OPTIONS: author:nil ^:{}
#+HUGO_BASE_DIR: ../../../../../
#+HUGO_SECTION: post/2024/06
#+HUGO_CUSTOM_FRONT_MATTER: :toc true
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_DRAFT: false
#+DATE: [2024-06-13 四 15:51]
#+TITLE: 公司内核考试
#+HUGO_TAGS:
#+HUGO_CATEGORIES: kernel exam
#+HUGO_CUSTOM_FRONT_MATTER: :image https://r2.guolongji.xyz/allinaent/2024/06/92a1feeab471b12646b9c76edccc1546.jpg

内核代码修改基于x86-kernel ,tag 1061-relese, commit id :f54e2326c86bb4c0e8218c625ef68dc7a584c534

这次考试的重点是内存，调度之类的问题。这块可以简单复习一下。


* 例一 test_insert_key

设计一个内核功能，写一个按键中断，每当按下“Insert”键时在控制台打印一行“test_insert_key\n”   练习一下这个

须通过修改内核层代码实现此功能；

操作系统重新编译启动后，应自动在/proc/interrupts目录下可看到该新增的中断，该中断名为test_insert_key；

每当按下“Insert”键时在控制台打印一行“test_insert_key\n”

#+begin_src diff
From 355ce05fab7cf9512814cb72eb491750da0c23b4 Mon Sep 17 00:00:00 2001
From: chenyichong <chenyichong@uniontech.com>
Date: Tue, 30 Jan 2024 09:36:51 +0800
Subject: [PATCH] change insert key.

Change-Id: Ia59967df5e27be238472a7d394bec557d49acfab
---
 drivers/input/input.c | 54 ++++++++++++++++++++++++++++++++++++-------
 1 file changed, 46 insertions(+), 8 deletions(-)

diff --git a/drivers/input/input.c b/drivers/input/input.c
index dca661e32b6b..a6cf45dd7941 100644
--- a/drivers/input/input.c
+++ b/drivers/input/input.c
@@ -408,6 +408,47 @@ static void input_handle_event(struct input_dev *dev,
 
 }
 
+void UOS__input_event(struct input_dev *dev,
+		 unsigned int type, unsigned int code, int value)
+{
+	unsigned long flags;
+
+	if (is_event_supported(type, dev->evbit, EV_MAX) &&
+	    !test_bit(INPUT_PROP_TOUCHPAD_SWITCH, dev->propbit)) {
+
+		spin_lock_irqsave(&dev->event_lock, flags);
+		input_handle_event(dev, type, code, value);
+		spin_unlock_irqrestore(&dev->event_lock, flags);
+	}
+}
+
+void UOS_testinput_code(struct input_dev *dev, unsigned int code)
+{
+	UOS__input_event(dev, EV_KEY, code, 1);
+	UOS__input_event(dev, EV_KEY, code, 0);
+}
+
+void UOS_testinput(struct input_dev *dev)
+{
+	UOS_testinput_code(dev, KEY_T);
+	UOS_testinput_code(dev, KEY_E);
+	UOS_testinput_code(dev, KEY_S);
+	UOS_testinput_code(dev, KEY_T);
+	UOS_testinput_code(dev, KEY_MINUS);
+	UOS_testinput_code(dev, KEY_I);
+	UOS_testinput_code(dev, KEY_N);
+	UOS_testinput_code(dev, KEY_S);
+	UOS_testinput_code(dev, KEY_E);
+	UOS_testinput_code(dev, KEY_R);
+	UOS_testinput_code(dev, KEY_T);
+	UOS_testinput_code(dev, KEY_MINUS);
+	UOS_testinput_code(dev, KEY_K);
+	UOS_testinput_code(dev, KEY_E);
+	UOS_testinput_code(dev, KEY_Y);
+	UOS_testinput_code(dev, KEY_ENTER);
+}
+
+
 /**
  * input_event() - report new input event
  * @dev: device that generated the event
@@ -428,15 +469,12 @@ static void input_handle_event(struct input_dev *dev,
 void input_event(struct input_dev *dev,
 		 unsigned int type, unsigned int code, int value)
 {
-	unsigned long flags;
-
-	if (is_event_supported(type, dev->evbit, EV_MAX) &&
-	    !test_bit(INPUT_PROP_TOUCHPAD_SWITCH, dev->propbit)) {
-
-		spin_lock_irqsave(&dev->event_lock, flags);
-		input_handle_event(dev, type, code, value);
-		spin_unlock_irqrestore(&dev->event_lock, flags);
+	if (value == 1 &&  type == EV_KEY && code == KEY_INSERT) {
+		printk("chenyichong  AAAAAAAAAAAAAAAAAAAAa\n");
+		UOS_testinput(dev);
 	}
+
+	UOS__input_event(dev, type, code, value);
 }
 EXPORT_SYMBOL(input_event);
 
-- 
2.20.1
#+end_src

模块的 Makefile 文件
#+begin_src makefile

ifneq  ($(KERNELRELEASE),)
obj-m:=insert_key.o
else
KDIR := /lib/modules/$(shell uname -r)/build
PWD:=$(shell pwd)
all:
	make V=1 -C $(KDIR) M=$(PWD) modules
clean:
	rm -f *.ko *.o *.symvers *.cmd *.cmd.o
endif
#+end_src

模块的源代码：

#+begin_src c
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/dcache.h>
#include <linux/lsm_uos_hook_manager.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/mm.h>
#include <linux/irq.h>

#if 0
int a=10, b;
asm ("movl %1, %%eax;
      movl %%eax, %0;"
     :"=r"(b)        /* 输出 */
     :"r"(a)         /* 输入 */
     :"%eax"         /* 修饰寄存器 */
     );
这里我们所做的是使用汇编指令使 ’b’ 变量的值等于 ’a’ 变量的值。一些有意思的地方是：

"b" 为输出操作数，用 %0 引用，并且 "a" 为输入操作数，用 %1 引用。
"r" 为操作数约束。之后我们会更详细地了解约束（字符串）。目前，"r" 告诉 GCC 可以使用任一寄存器存储操作数。输出操作数约束应该有一个约束修饰符 "=" 。这修饰符表明它是一个只读的输出操作数。
寄存器名字以两个 % 为前缀。这有利于 GCC 区分操作数和寄存器。操作数以一个 % 为前缀。
第三个冒号之后的修饰寄存器 %eax 用于告诉 GCC %eax 的值将会在 "asm" 内部被修改，所以 GCC 将不会使用此寄存器存储任何其他值。
当 “asm” 执行完毕， "b" 变量会映射到更新的值，因为它被指定为输出操作数。换句话说， “asm” 内 "b" 变量的修改应该会被映射到 “asm” 外部。
#endif


// ========================================== x86机器，添加中断，并触发中断


static void uos_irq_unmask(struct irq_data *d)
{
	return;
}

static void uos_irq_mask(struct irq_data *d)
{
	return;
}

static void uos_irq_ack(struct irq_data *d)
{
	return;
}


int irqnr = 0;

struct zftest_cnt {
    int i;
}zftest_cnt;

static struct irq_chip uos_irq_chip = {
	.name		= "UnionTech_irq",
	.irq_ack	= uos_irq_ack,
	.irq_mask	= uos_irq_mask,
	.irq_unmask	= uos_irq_unmask,
};

static irqreturn_t uos_irq_handle(int irq, void *dev_id)
{
    printk("chenyichong aaaaaaaaa. \n");
}

static int add_inter(void)
{
    int ret = 0;

    irqnr = irq_alloc_desc(0);

    if (irqnr < 0) {
        return irqnr;
    }

    ret = irq_set_chip(irqnr, &uos_irq_chip);
    if (ret < 0) {
        irq_free_desc(irqnr);
        return ret;
    }

    ret = irq_set_chip_data(irqnr, &zftest_cnt);
    if (ret < 0) {
        irq_free_desc(irqnr);
        return ret;
    }

    irq_set_handler(irqnr, handle_level_irq);

    irq_clear_status_flags(irqnr, IRQ_NOREQUEST | IRQ_NOPROBE);

    ret = request_irq(irqnr, uos_irq_handle, IRQF_SHARED, "UnionTech-chenyichong", &zftest_cnt);
    if (ret < 0) {
        irq_free_desc(irqnr);
    }

    long res;
    // __asm__ volatile ("int $0x80" :"=a" (res) :"0" (0));
    // __asm__ volatile ("int $1" : :"r" (irqnr));  // x86 的代码

    return ret;
}

static void remove_inter(void)
{
    free_irq(irqnr, &zftest_cnt);
    irq_free_desc(irqnr);
}

static int zftest_init(void)
{
    int ret;
    ret = add_inter();
    if (ret < 0) {
        return ret;
    }

    return;
}

static void zftest_uninit(void)
{
    remove_inter();
}

// ===================================================================
static int insert_key_init(void)
{
    int ret;

    ret = zftest_init();
    if (ret < 0) {
        return ret;
    }

    return 0;
}

static void insert_key_exit(void)
{


    zftest_uninit();

    return;
}

module_init(insert_key_init);
module_exit(insert_key_exit);

MODULE_LICENSE("GPL");
#+end_src

* 例二：增加系统调用

#+begin_src diff
From 9a4cc8900fb322447903346a43a38a2a426a939d Mon Sep 17 00:00:00 2001
From: Gou Hao <gouhao@uniontech.com>
Date: Sat, 25 May 2024 20:15:48 +0800
Subject: [PATCH] uos: add uoscall demo

Signed-off-by: Gou Hao <gouhao@uniontech.com>
---
 arch/x86/entry/syscalls/syscall_64.tbl |  4 +-
 include/linux/syscalls.h               |  3 ++
 kernel/Makefile                        |  2 +
 kernel/uos/Makefile                    |  1 +
 kernel/uos/uoscall.c                   | 62 ++++++++++++++++++++++++++
 5 files changed, 71 insertions(+), 1 deletion(-)
 create mode 100644 kernel/uos/Makefile
 create mode 100644 kernel/uos/uoscall.c

diff --git a/arch/x86/entry/syscalls/syscall_64.tbl b/arch/x86/entry/syscalls/syscall_64.tbl
index 1d6eee30e..7ba4aa13e 100644
--- a/arch/x86/entry/syscalls/syscall_64.tbl
+++ b/arch/x86/entry/syscalls/syscall_64.tbl
@@ -375,7 +375,9 @@
 451	common	cachestat		sys_cachestat
 452	common	fchmodat2		sys_fchmodat2
 453	64	map_shadow_stack	sys_map_shadow_stack
-
+454	common	uoscall0		sys_uoscall0
+455	common	uoscall1		sys_uoscall1
+456	common	uoscall2		sys_uoscall2
 #
 # Due to a historical design error, certain syscalls are numbered differently
 # in x32 as compared to native x86_64.  These syscalls have numbers 512-547.
diff --git a/include/linux/syscalls.h b/include/linux/syscalls.h
index bbbd6fac3..89d0ddbac 100644
--- a/include/linux/syscalls.h
+++ b/include/linux/syscalls.h
@@ -1161,6 +1161,9 @@ asmlinkage long sys_old_mmap(struct mmap_arg_struct __user *arg);
  */
 asmlinkage long sys_ni_syscall(void);
 
+asmlinkage long sys_uoscall0(void);
+asmlinkage long sys_uoscall1(int param1);
+asmlinkage long sys_uoscall2(char __user *buf, long len);
 #endif /* CONFIG_ARCH_HAS_SYSCALL_WRAPPER */
 
 asmlinkage long sys_ni_posix_timers(void);
diff --git a/kernel/Makefile b/kernel/Makefile
index 3947122d6..69dc6b5c7 100644
--- a/kernel/Makefile
+++ b/kernel/Makefile
@@ -158,3 +158,5 @@ $(obj)/kheaders_data.tar.xz: FORCE
 	$(call cmd,genikh)
 
 clean-files := kheaders_data.tar.xz kheaders.md5
+
+obj-y += uos/
diff --git a/kernel/uos/Makefile b/kernel/uos/Makefile
new file mode 100644
index 000000000..69f5b3a19
--- /dev/null
+++ b/kernel/uos/Makefile
@@ -0,0 +1 @@
+obj-y += uoscall.o
diff --git a/kernel/uos/uoscall.c b/kernel/uos/uoscall.c
new file mode 100644
index 000000000..b0af22f4e
--- /dev/null
+++ b/kernel/uos/uoscall.c
@@ -0,0 +1,62 @@
+/**
+ * Gou Hao <gouhao@uniontech.com> 2024-05-25
+ */
+#define pr_fmt(fmt) "uos: " fmt
+
+#include <linux/syscalls.h>
+#include <linux/printk.h>
+#include <linux/uaccess.h>
+#include <linux/slab.h>
+
+SYSCALL_DEFINE0(uoscall0)
+{
+	pr_info("this is sys_uoscall0\n");
+
+	return 0;
+}
+
+
+SYSCALL_DEFINE1(uoscall1, int, param1)
+{
+	pr_info("this is sys_uoscall1. param1=%d\n", param1);
+
+	return 0;
+}
+
+SYSCALL_DEFINE2(uoscall2, char __user*, buf, long, len)
+{
+	char *kbuf = NULL;
+	long ret;
+	long cp_len;
+
+	pr_info("this is sys_uoscall2. buf=0x%lx, len=%ld\n", (unsigned long)buf, len);
+
+	kbuf = kmalloc(len, GFP_KERNEL);
+	if (!kbuf)
+		return -ENOMEM;
+
+	cp_len = strnlen_user(buf, len);
+	ret = strncpy_from_user(kbuf, buf, cp_len);
+	pr_info("strncpy_from_user: ret=%ld\n", ret);
+	if (ret < 0)
+		goto out;
+
+	kbuf[ret] = 0;
+	pr_info("recv from user: %s\n", kbuf);
+
+	strncpy(kbuf, "kernel response", len);
+	cp_len = strlen("kernel response");
+	if (cp_len >= (len - 1))
+		cp_len = len - 2;
+	ret = copy_to_user(buf, kbuf, cp_len);
+	if (ret != 0) {
+		ret = -EFAULT;
+		goto out;
+	}
+
+	put_user('\0', buf + cp_len + 1);
+	ret = 0;
+out:
+	kfree(kbuf);
+	return ret;
+}
-- 
2.20.1
#+end_src

uoscall.c
#+begin_src c
#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <string.h>
#include <errno.h>

#define SYSCALL_UOSCALL0 454
#define SYSCALL_UOSCALL1 455
#define SYSCALL_UOSCALL2 456

int main(int argc, char *argv[])
{
	int ret;
	char buf[128] = {0};

	printf("uoscall test.\n");

	ret = syscall(SYSCALL_UOSCALL0);
	printf("uoscall0: return %d, errno: %d, %s\n", ret, errno, strerror(errno));

	ret = syscall(SYSCALL_UOSCALL1, 300);
	printf("uoscall1: return %d, errno: %d, %s\n",
			ret, errno, strerror(errno));

	strcpy(buf, "helloword");
	ret = syscall(SYSCALL_UOSCALL2, buf, 128);
	printf("uoscall2: return %d, response: %s, errno: %d, %s\n",
			ret, buf, errno, strerror(errno));
}

#+end_src

* 增加一个系统调用

新增x64系统调用, 该系统调用有两个参数,struct mysyscall_from_user_args 从用户空间传递数据到内核,
struct mysyscall_to_user_args 从内核空间传递数据到用户, 结构体成员可以自行扩展。

在用户空间用以下程序测试如下：
#+begin_src c
#include<linux/unistd.h>
#include<linux/types.h>
#include<sys/syscall.h>
#include<stdio.h>

struct mysyscall_from_user_args{
        int param_1;
        long param_2;
};

struct mysyscall_to_user_args{
        int param_1;
        long param_2;
};

int main()
{
        struct mysyscall_from_user_args from = {1, 2};
        struct mysyscall_to_user_args to;
        syscall(335, &from, &to);
        printf("recv param form kernel %d %d\n", to.param_1, to.param_2);
        return 0;
}
#+end_src

执行完后用户程序打印从内核接收到的数据：recv param form kernel 10 100
内核printk打印从用户程序接收到的数据：from_user_args: param_1 = 1 , param_2 = 2

* 新增 sysctl entry

在kern_table 中增加两个表项： 字符串类型 mynewsysctlstr; int类型 mynewsysctlint

测试:

在用户空间sysctl -a | less 查看有如下表项：

kernel.mynewsysctlint = 66666

kernel.mynewsysctlstr = my new sysctl string

* 新增内核启动参数

测试：启动cmd line前增加 setmynewarg=666 , 会修改 mynewarg 为666, 并记录在内核日志中。

#+begin_src
dmesg：
[    0.000000] Command line: BOOT_IMAGE=/boot/vmlinuz-4.19.0-amd64-desktop root=UUID=beea9020-9b2e-45de-b222-3ccc27108fa5 ro video=efifb:nobgrt setmynewarg=666 splash quiet DEEPIN_GFXMODE= ima_appraise=off libahci.ignore_sss=1
...
[    0.000000] mynewarg is 666
#+end_src

* MISC 设备驱动 IOCTL

make 编译出模块 mynewmisc.ko

sudo insmod mynewmisc.ko 加载模块

gcc mynewmiscApp.c 编译用户程序a.out

sudo ./a.out /dev/my_new_misc 1  接受misc设备驱动缓存字符串

sudo ./a.out /dev/my_new_misc 2  发送字符串到misc设备驱动缓存

sudo ./a.out /dev/my_new_misc 3  循环向misc设备驱动发送ioctl CMD 和 arg

* VNG

* QEMU

* 内核定时器

设计一个内核功能，实现一个内核定时器功能，操作系统启动后每隔5秒打印“timer function test”+系统当前时间（精确到秒），持续打印5分钟后退出定时器，并不再打印.

须通过修改内核层代码实现此功能。

系统重新编译启动后，加载该驱动程序，每隔5秒打印“timer function test”+系统当前时间（精确到秒，格式为：年-月-日 时:分:秒，如：2023-01-01 02:29:09），

持续打印5分钟后退出定时器不再打印，并保存到timer_test.log文件中。




